﻿using Blog_MVC.Models;
using Blog_MVC.Models.ClassAux;
using Blog_MVC.Models.TableViewModels;
using Blog_MVC.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Blog_MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<PostTableViewModel> lst = null;
            using(var db = new BlogEntities())
            {
                lst = (from d in db.Post
                       where d.Post_State == true
                       orderby d.Post_Creation_Date descending
                       select new PostTableViewModel
                       {
                           title = d.Post_Title,
                           id = d.Id_P
                       }).ToList();
            }
            //asdsa uiasdui a
            //commiteando el un brancho
            // luego hacer merge
            //estoy en branch develop y voy a comitear
            return View(lst);
        }

        [HttpGet]
        public ActionResult Add()
        {

            oList obj = new oList();
            using (var db = new BlogEntities())
            {
                obj.lstCat = (from d in db.Category
                       where d.Category_State == true
                       orderby d.Category_Name
                       select new CatTableViewModel
                       {
                           id_c = d.Id_C,
                           name_c = d.Category_Name
                       }).ToList();
            }
            return View(obj);
        }

        [HttpPost]
        public ActionResult Add(oList model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var db = new BlogEntities())
            {
                Post oPost = new Post();
                oPost.Post_State = true;
                oPost.Post_Title = model.modelAdd.title;
                oPost.Post_Content = model.modelAdd.content;
                oPost.Post_Image = "Imagenm.jpg";
                oPost.Post_Category_Id = model.valueDdl;
                oPost.Post_Creation_Date = DateTime.Now;


                db.Post.Add(oPost);
                db.SaveChanges();
            }
            return Redirect(Url.Content("~/Home/"));

        }

        public ActionResult ViewPost(int Id)
        {

            var model = new PostViewModel();
            using (var db = new BlogEntities())
            {
                var oPost = db.Post.Find(Id);
                model.title = oPost.Post_Title;
                model.content = oPost.Post_Content;
                model.category = oPost.Post_Category_Id;
                var oCat = db.Category.Find(model.category);
                model.sCategory = oCat.Category_Name;
                model.date = oPost.Post_Creation_Date;
            }

            return View(model);
        }

        public ActionResult Edit(int Id)
        {
            var model = new EditPostViewModel();

            using (var db = new BlogEntities())
            {
                var oPost = db.Post.Find(Id);
                model.title = oPost.Post_Title;
                model.content = oPost.Post_Content;
                model.category = oPost.Post_Category_Id;
                var oCat = db.Category.Find(model.category);
                model.sCategory = oCat.Category_Name;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditPostViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var db = new BlogEntities())
            {
                
                
                var oPost = db.Post.Find(model.Id);
                oPost.Post_State = true;
                oPost.Post_Title = model.title;
                oPost.Post_Content = model.content;
              
                


                db.Entry(oPost).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            return Redirect(Url.Content("~/Home/"));

        }

        public ActionResult Delete(int Id)
        {
            using (var db = new BlogEntities())
            {
                var oPost = db.Post.Find(Id);
                oPost.Post_State = false;

                db.Entry(oPost).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

            }

            return Content("1");
        }

        public ActionResult Detail(string word)
        {

            if(word == null || word == "")
            {
                return RedirectToAction("Index", "Home");
            }
            List<PostTableViewModel> lst = null;
                using (var db = new BlogEntities())
                {
                    try
                    {
                        lst = (from d in db.Post
                               join s in db.Category on d.Post_Category_Id equals s.Id_C
                               where d.Post_State == true && d.Post_Title.ToString().Contains(word)
                               select new PostTableViewModel
                               {
                                   id = d.Id_P,
                                   title = d.Post_Title,
                                   content = d.Post_Content,
                                   category = s.Category_Name,
                                   date = d.Post_Creation_Date

                               }).ToList();
                        ViewBag.Ok = "Paso correctamente";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Ocurrio un problema" + ex.Message;
                    }
                }
                return View(lst);
            

        }

    }
}