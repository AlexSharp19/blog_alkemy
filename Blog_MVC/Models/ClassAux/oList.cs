﻿using Blog_MVC.Models.TableViewModels;
using Blog_MVC.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_MVC.Models.ClassAux
{
    public class oList
    {
        public PostViewModel modelAdd { get; set; }

        public ICollection<CatTableViewModel> lstCat { get; set; }

        public string carSelect { get; set; }

        public int valueDdl { get; set; }

    }
}