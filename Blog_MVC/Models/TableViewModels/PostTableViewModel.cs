﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_MVC.Models.TableViewModels
{
    public class PostTableViewModel
    {
        public int id { get; set; }

        public string title { get; set; }

        public string content { get; set; }

        public string image { get; set; }

       
        public string category { get; set; }

       
        public DateTime date { get; set; }

    }
}