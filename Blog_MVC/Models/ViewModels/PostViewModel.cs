﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_MVC.Models.ViewModels
{
    public class PostViewModel
    {
        [Required]
        [Display(Name="Titulo")]
        public string title { get; set; }

        [Required]
        [StringLength(350, ErrorMessage = "El {0} debe tener maximo {1} caracteres")]
        [Display(Name = "Contenido")]
        public string content { get; set; }

        public string image { get; set; }

        [Required]
        [Display(Name = "Categoria")]
        public int category { get; set; }

        [Display(Name = "Categoria")]
        public string sCategory { get; set; }

        [Display(Name = "Fecha")]
        public DateTime date { get; set; }

        //public PostViewModel()
        //{
        //    using (var db = new BlogEntities())
        //    {
        //        Post oPost = new Post();
        //        oPost.Post_Title = this.title;
        //    }
        //}

    }

    public class EditPostViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Titulo")]
        public string title { get; set; }

        [Required]
        [StringLength(350, ErrorMessage = "El {0} debe tener maximo {1} caracteres")]
        [Display(Name = "Contenido")]
        public string content { get; set; }

        public string image { get; set; }
        
        public int category { get; set; }

        [Display(Name = "Categoria")]
        public string sCategory { get; set; }

        public DateTime date { get; set; }

    }

}